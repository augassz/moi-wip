﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public enum GameState { GameMenu, GamePlay, GameSave, GameLoad, GameOver, GameExit }
    private GameState game_state;

    private bool bGameLoop;

    public static GameManager instance = null;

    // private PlayersManager players; // for future ?
    bool bMapUpdate;
    private MapManager mapManager;
    private PartiesManager partiesManager;
    private PlayerControl playerControl;

    private ObjectSelectionLook selectionLook;

    //Awake is always called before any Start functions
    void Awake() {
        bGameLoop = false;

        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        InitGame();
    }

    void InitGame() {

        selectionLook = ScriptableObject.CreateInstance<ObjectSelectionLook>();

        game_state = GameState.GameMenu;

        mapManager = new MapManager(100, 100);

        partiesManager = new PartiesManager();

        CreateHumanParty();
        CreateAIParty();

        playerControl = gameObject.AddComponent<PlayerControl>();
        ChangeGameState(GameState.GamePlay);

        bMapUpdate = true;
        StartCoroutine(MapUpdate());
    }

    IEnumerator MapUpdate() {

        while (bMapUpdate) {

            mapManager.UpdateCurrentMap();
            yield return null;
        }
    }

    void Update() {

        if (!bGameLoop)
            return;

        playerControl.CustomUpdate();


    }

    public ObjectSelectionLook GetSelectionLook() {
        return selectionLook;
    }

    public void ChangeGameState(GameState gamestate) {
        game_state = gamestate;

        switch (game_state) {

            case GameState.GameMenu:
                bGameLoop = false;

                break;

            case GameState.GamePlay:
                bGameLoop = true;

                break;

            case GameState.GameSave:
                break;

            case GameState.GameLoad:
                break;

            case GameState.GameOver:
                break;

            case GameState.GameExit:
                break;
        }

    }



    public MapManager GetMapManager() {
        return mapManager;
    }

    public PlayerControl GetPlayerControl() {
        return playerControl;
    }

    private void CreateHumanParty() {

        Party newParty = new Party(true, "HumanParty", 0);

        UnitCreator creator = ScriptableObject.CreateInstance<UnitCreator>();
        Unit starterCharacter = creator.CreateUnit(0, 0);
        Unit starterCharacter1 = creator.CreateUnit(0, 1);
        Unit starterCharacter2 = creator.CreateUnit(1, 0);

        newParty.AddUnitToParty(starterCharacter);
        newParty.AddUnitToParty(starterCharacter1);
        newParty.AddUnitToParty(starterCharacter2);

        partiesManager.AddParty(newParty);
    }

    private void CreateAIParty() {

        Party newParty = new Party(false, "AIParty", -100);

        UnitCreator creator = ScriptableObject.CreateInstance<UnitCreator>();
        Unit starterCharacter = creator.CreateEnemy(5, 5);
        Unit starterCharacter1 = creator.CreateEnemy(6, 6);

        newParty.AddUnitToParty(starterCharacter);
        newParty.AddUnitToParty(starterCharacter1);

        partiesManager.AddParty(newParty);


    }

    void OnDestroy() {
        bMapUpdate = false;
    }
}