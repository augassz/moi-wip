﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartiesManager {

    private List<Party> partiesList;
    private int idOfParty;

    public PartiesManager() {
        partiesList = new List<Party>();
        LoadParties();
    }

    public List<Party> GetParties() {
        return partiesList;
    }

    public void AddParty(Party party) {
        partiesList.Add(party);
    }

    public void RemoveParty(Party party) {
        partiesList.Remove(party);
    }

    public void RefreshParties() {
        foreach (Party party in partiesList) {
            party.RefreshUnitsEnergy();
        }
    }

    private void SaveParties() {

    }

    private void LoadParties() {

    }
}
