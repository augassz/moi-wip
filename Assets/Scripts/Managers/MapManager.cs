﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager {

    private List<MapClass> mapList;
    private int idOfMap;

    public MapManager(int mapXSize, int mapYsize) {
        mapList = new List<MapClass>();
        LoadMaps();

        CreateNewMap(mapXSize, mapYsize);
    }

    public MapManager(int mapIndex) {
        mapList = new List<MapClass>();
        LoadMaps();

        idOfMap = mapIndex;
        LoadMap(mapIndex);
    }

    public MapClass GetCurrenMap() {
        return mapList[idOfMap];
    }

    public void UpdateCurrentMap() {
        mapList[idOfMap].UpdateMapTilesOccupancy();
    }

    public void CreateNewMap(int mapXSize, int mapYSize) {
        MapClass newMap = new MapClass();
        newMap.CreateMap(mapXSize, mapYSize);
        mapList.Add(newMap);
        idOfMap = mapList.IndexOf(newMap);
    }

    public void LoadMap(int mapIndex) {

    }

    private void SaveMap() {

    }

    private void LoadMaps() {

    }






}
