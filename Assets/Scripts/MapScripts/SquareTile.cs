﻿using UnityEngine;

public class SquareTile : ScriptableObject {

    private int X;
    private int Y;
    private int id;

    Vector2 worldPosition;

    private bool bOccupied;
    public SquareTile parent;
    public int gCost;
    public int hCost;

    private GameObject tilePrefab;
    private GameObject tile;

    public static GameObject tiles;

    private void Awake() {
        if (!tiles) {
            tiles = new GameObject();
            tiles.name = "Tiles";
        }
    }

    public void Init(int i, int x, int y, Vector2 worldPos) {
        tilePrefab = (GameObject)Resources.Load("Tile");
        id = i;
        X = x;
        Y = y;

        worldPosition = worldPos;
        Select();
    }

    public void testing() {

        bOccupied = false;
        Vector3 origin = new Vector3(worldPosition.x, 0.5f, worldPosition.y);
        float thickness = 0.5f;

        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = origin;
        sphere.transform.localScale = new Vector3(thickness, thickness, thickness);
    }

    public void UpdateIfOcupied() {

        bOccupied = false;
        Vector3 origin = new Vector3(worldPosition.x, 0.5f, worldPosition.y);
        float thickness = 0.3f;

        Collider[] hitColliders = Physics.OverlapSphere(origin, thickness);

        if (hitColliders.Length > 0)
            bOccupied = true;
    }

    public int fCost {
        get {
            return gCost + hCost;
        }
    }

    public int GetId() {
        return id;
    }

    public Vector2 GetPosition() {
        return new Vector2(X, Y);
    }

    public Vector2 GetWorldPosition() {
        return worldPosition;
    }

    public void Select() {

        if (tile)
            return;

        tile = Instantiate(tilePrefab, new Vector3(worldPosition.x, 0.01f, worldPosition.y), Quaternion.Euler(90.0f, 0.0f, 0.0f));
        tile.transform.parent = tiles.transform;

        //Material mat = tile.GetComponent<Renderer>().material;
        //mat.color = Color.white;
        //tile.GetComponent<Renderer>().material = mat;
    }

    public void SelectPath() {

        if (!tile) {
            tile = Instantiate(tilePrefab, new Vector3(worldPosition.x, 0.01f, worldPosition.y), Quaternion.Euler(90.0f, 0.0f, 0.0f));
            tile.transform.parent = tiles.transform;
        }

        Material mat = tile.GetComponent<Renderer>().material;
        mat.color = Color.green;
        tile.GetComponent<Renderer>().material = mat;
    }

    public void Deselect() {
        if (!tile)
            return;

        Destroy(tile);
    }

    public bool CheckIfOccupied() {

        return bOccupied;
    }

   

}
