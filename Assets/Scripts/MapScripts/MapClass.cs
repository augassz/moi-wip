﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapClass {

    private int mapSizeX;
    private int mapSizeY;

    private MapTilesCreator hexMapMaker;
    private Dictionary<Vector2, SquareTile> gameTileMap;

    public MapClass() {
        hexMapMaker = new MapTilesCreator();
        gameTileMap = new Dictionary<Vector2, SquareTile>();
    }

    public void CreateMap(int xSize, int ySize) {
        mapSizeX = xSize;
        mapSizeY = ySize;
        // create how map looks and other stuff
        gameTileMap = hexMapMaker.CreateMapGrid(xSize, ySize);
    }

    public void UpdateMapTilesOccupancy() {

        foreach(SquareTile tile in gameTileMap.Values) {
            tile.UpdateIfOcupied();
        }
    }

    public Dictionary<Vector2, SquareTile> GetMap() {
        return gameTileMap;
    }

    public List<SquareTile> GetNeighbours(SquareTile tile) {

        List<SquareTile> neighbours = new List<SquareTile>();

        if (!tile)
            return neighbours;

        SquareTile foundTile = null;
        Vector2 tileCoords = tile.GetPosition();
        Vector2 coords = new Vector2();

        coords = new Vector2(tileCoords.x, tileCoords.y + 1);
        if (coords.x >= 0 && coords.y >= 0 && coords.x < mapSizeX && coords.y < mapSizeY) {
            gameTileMap.TryGetValue(coords, out foundTile);
            if (foundTile)
                neighbours.Add(foundTile);
        }

        foundTile = null;
        coords = new Vector2(tileCoords.x + 1, tileCoords.y + 1);
        if (coords.x >= 0 && coords.y >= 0 && coords.x < mapSizeX && coords.y < mapSizeY) {
            gameTileMap.TryGetValue(coords, out foundTile);
            if (foundTile)
                neighbours.Add(foundTile);
        }

        foundTile = null;
        coords = new Vector2(tileCoords.x + 1, tileCoords.y);
        if (coords.x >= 0 && coords.y >= 0 && coords.x < mapSizeX && coords.y < mapSizeY) {
            gameTileMap.TryGetValue(coords, out foundTile);
            if (foundTile)
                neighbours.Add(foundTile);
        }

        foundTile = null;
        coords = new Vector2(tileCoords.x + 1, tileCoords.y - 1);
        if (coords.x >= 0 && coords.y >= 0 && coords.x < mapSizeX && coords.y < mapSizeY) {
            gameTileMap.TryGetValue(coords, out foundTile);
            if (foundTile)
                neighbours.Add(foundTile);
        }

        foundTile = null;
        coords = new Vector2(tileCoords.x, tileCoords.y - 1);
        if (coords.x >= 0 && coords.y >= 0 && coords.x < mapSizeX && coords.y < mapSizeY) {
            gameTileMap.TryGetValue(coords, out foundTile);
            if (foundTile)
                neighbours.Add(foundTile);
        }

        foundTile = null;
        coords = new Vector2(tileCoords.x - 1, tileCoords.y - 1);
        if (coords.x >= 0 && coords.y >= 0 && coords.x < mapSizeX && coords.y < mapSizeY) {
            gameTileMap.TryGetValue(coords, out foundTile);
            if (foundTile)
                neighbours.Add(foundTile);
        }

        foundTile = null;
        coords = new Vector2(tileCoords.x - 1, tileCoords.y);
        if (coords.x >= 0 && coords.y >= 0 && coords.x < mapSizeX && coords.y < mapSizeY) {
            gameTileMap.TryGetValue(coords, out foundTile);
            if (foundTile)
                neighbours.Add(foundTile);
        }

        foundTile = null;
        coords = new Vector2(tileCoords.x - 1, tileCoords.y + 1);
        if (coords.x >= 0 && coords.y >= 0 && coords.x < mapSizeX && coords.y < mapSizeY) {
            gameTileMap.TryGetValue(coords, out foundTile);
            if (foundTile)
                neighbours.Add(foundTile);
        }

        return neighbours;
    }
}
