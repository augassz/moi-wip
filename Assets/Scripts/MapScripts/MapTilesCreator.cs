﻿using System.Collections.Generic;
using UnityEngine;

public class MapTilesCreator {

    [SerializeField] private int X;
    [SerializeField] private int Y;

    public MapTilesCreator() { }

    public Dictionary<Vector2, SquareTile> CreateMapGrid(int xSize, int ySize) {

        X = xSize;
        Y = ySize;

        float xOffset = 1.5f;
        float yOffset = 1.5f;

        Dictionary<Vector2, SquareTile> map = new Dictionary<Vector2, SquareTile>();

        int id = 0;
        for (int i = 0; i < X; i++) {
            for (int j = 0; j < Y; j++) {
                SquareTile tile = ScriptableObject.CreateInstance<SquareTile>();
                tile.Init(id++, i, j, new Vector2(i * xOffset, j * yOffset));
                map.Add(new Vector2(i, j), tile);
            }
        }
        Debug.Log("Sukurta " + map.Count + " map tiles");

        return map;
    }

}