﻿using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class PathfinderClass {

    Thread pathFinderThread;
    UnitControler unitControler;
    List<SquareTile> path;

    SquareTile start, finish;

    public PathfinderClass() { }

    public void FindPath(UnitControler u, SquareTile s, SquareTile f) {
        unitControler = u;
        start = s;
        finish = f;
        path = new List<SquareTile>();

        pathFinderThread = new Thread(AStar);
        pathFinderThread.Start();
    }

    void SendResult() {

        if(path.Count == 0) {
            return;
        }
        unitControler.SetPath(path);
        unitControler = null;
    }

    void AStar() {

        List<SquareTile> openSet = new List<SquareTile>();
        HashSet<SquareTile> closedSet = new HashSet<SquareTile>();
        openSet.Add(start);

        while (openSet.Count > 0) {

            SquareTile currentHex = openSet[0];

            for (int i = 1; i < openSet.Count; i++) {
                if (openSet[i].fCost < currentHex.fCost || openSet[i].fCost == currentHex.fCost) {
                    if (openSet[i].hCost < currentHex.hCost)
                        currentHex = openSet[i];
                }
            }

            openSet.Remove(currentHex);
            closedSet.Add(currentHex);

            if (currentHex == finish) {
                path = RetracePath(start, finish);
                SendResult();
                Thread.CurrentThread.Abort();
            }

            foreach (SquareTile neighbour in GameManager.instance.GetMapManager().GetCurrenMap().GetNeighbours(currentHex)) {

                if (neighbour.CheckIfOccupied() || closedSet.Contains(neighbour)) {
                    continue;
                }

                int newCostToNeighbour = currentHex.gCost + GetDistance(currentHex, neighbour);
                if (newCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour)) {
                    neighbour.gCost = newCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, finish);
                    neighbour.parent = currentHex;

                    if (!openSet.Contains(neighbour))
                        openSet.Add(neighbour);
                }
            }
        }

    }

    private List<SquareTile> RetracePath(SquareTile startHex, SquareTile endHex) {

        List<SquareTile> path = new List<SquareTile>();
        SquareTile currentNode = endHex;

        while (currentNode != startHex) {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();

        return path;
    }

    private int GetDistance(SquareTile tileA, SquareTile tileB) {
        if (!tileA || !tileB)
            return 0;

        int dstX = Mathf.Abs((int)tileA.GetPosition().x - (int)tileB.GetPosition().x);
        int dstY = Mathf.Abs((int)tileA.GetPosition().y - (int)tileB.GetPosition().y);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }
}
