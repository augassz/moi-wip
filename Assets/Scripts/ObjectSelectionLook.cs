﻿using UnityEngine;

public class ObjectSelectionLook : ScriptableObject {

    GameObject unitSelectionPrefab;
    GameObject unitSelection;

    void Awake() {
        unitSelectionPrefab = (GameObject) Resources.Load("SelectedLook");
    }

    public GameObject ShowSelected(Vector3 position) {
        Vector3 yZeroPos = new Vector3(position.x, 0.01f, position.z);
        GameObject gO = Instantiate(unitSelectionPrefab, yZeroPos, Quaternion.Euler(90.0f, 0.0f, 0.0f));
        return gO;
    }
}
