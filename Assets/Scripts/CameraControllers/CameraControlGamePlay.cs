﻿using UnityEngine;

public class CameraControlGamePlay : MonoBehaviour {

    public float camSpeed = 20f;
    public float borderThickness = 15f;
    public Vector2 limits;

    public float scrollSpeed = 20f;
    public float minY = 15f;
    public float maxY = 25f;   

    void Update() {

        Vector3 cameraPos = transform.position;

        if(Input.mousePosition.y >= Screen.height - borderThickness) {
            cameraPos.z += camSpeed * Time.deltaTime;
        }

        if (Input.mousePosition.y <= borderThickness) {
            cameraPos.z -= camSpeed * Time.deltaTime;
        }

        if (Input.mousePosition.x >= Screen.width - borderThickness) {
            cameraPos.x += camSpeed * Time.deltaTime;
        }

        if (Input.mousePosition.x <= borderThickness) {
            cameraPos.x -= camSpeed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        cameraPos.y -= scroll * scrollSpeed * 100f * Time.deltaTime;

        cameraPos.x = Mathf.Clamp(cameraPos.x, -limits.x, limits.x);
        cameraPos.y = Mathf.Clamp(cameraPos.y, minY, maxY);
        cameraPos.z = Mathf.Clamp(cameraPos.z, -limits.y, limits.y);

        transform.position = cameraPos;
    }

}
