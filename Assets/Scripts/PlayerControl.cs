﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    private string playerPartyName = "HumanParty";

    private MapClass currentMap;
    private Unit selectedUnit;

    private SquareTile tileToMove;
    private bool bTileSelector;

    private bool bRightClicked;

    void Start() {

        currentMap = GameManager.instance.GetMapManager().GetCurrenMap();
        bTileSelector = false;
        selectedUnit = null;
        bRightClicked = false;
    }

    public void CustomUpdate() {

        if (Input.GetMouseButtonDown(0))
            LeftClick();



        if (selectedUnit != null) {

            if (!bTileSelector) {
                bTileSelector = true;
                StartCoroutine(TileSelection());
            }

        }
        else {
            bTileSelector = false;
        }



        if (Input.GetMouseButtonDown(1) && !bRightClicked) {
            RightClick();
        }

    }
    
    // unit has target
    // if unit doesnt have target, it cant attack or use magic
    // 

    private void LeftClick() {
        RaycastSelect();
    }

    private void RightClick() {

        bRightClicked = true;

        if (selectedUnit == null) {
            Debug.LogError("Unit not selected.");
            bRightClicked = false;
            return;
        }

        selectedUnit.MoveUnit(tileToMove);

        bRightClicked = false;
    }

    private float distance = 100f;

    private void RaycastSelect() {

        if (selectedUnit) {
            selectedUnit.DeselectUnit();
            selectedUnit = null;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, distance)) {

            Vector2 mousePoint = new Vector2(hit.point.x, hit.point.z);

            InteractiveObject interactiveObj = hit.collider.GetComponent<InteractiveObject>();    // issiaiskinam koks object \/
            if (interactiveObj) {

                if (Equals(interactiveObj.GetInteractiveObjectType().GetType(), typeof(Unit))) { // Ar unit tipo
                    Unit unit = (Unit) interactiveObj.GetInteractiveObjectType();

                    if (unit.unitParty.partyName == playerPartyName) {
                        selectedUnit = unit;
                        selectedUnit.SelectUnit();
                    }

                }



            }    


        }
    }

    private IEnumerator TileSelection() {

        while (bTileSelector) {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, distance)) {

                Vector2 mousePoint = new Vector2(hit.point.x, hit.point.z);


                SquareTile selectedTile = null;
                Vector2 tilePos = new Vector2(mousePoint.x / 1.5f, mousePoint.y / 1.5f);

                Debug.Log(tilePos);

                tilePos = new Vector2(Mathf.Round(tilePos.x) , Mathf.Round(tilePos.y) );

                currentMap.GetMap().TryGetValue(tilePos, out selectedTile);

                if (selectedTile && !selectedTile.CheckIfOccupied()) {
                    tileToMove = selectedTile;
                }

            }

            yield return new WaitForSeconds(0.01f);
        }

    }


}
