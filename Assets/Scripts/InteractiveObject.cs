﻿using UnityEngine;

public class InteractiveObject : MonoBehaviour {

	public Component GetInteractiveObjectType() {

        foreach (Component c in GetComponents(typeof(Component))) {
            if (c.GetType().Equals(typeof(Unit)))
                return c;
        }

        return null;
    }

}
