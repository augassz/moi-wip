﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitControler : MonoBehaviour  {

    private float moveSpeed = 2.0f;
    private float rotationSpeed = 7.0f;

    Unit unitComponent;

    private MapClass currentMap;
    List<SquareTile> moveList;
    Vector3 positionToGo;
    SquareTile currentTarget;

    private PathfinderClass pathfinder;

    void Start () {

        unitComponent = GetComponent<Unit>();

        pathfinder = new PathfinderClass();
        moveList = new List<SquareTile>();
        positionToGo = new Vector3();
        currentMap = GameManager.instance.GetMapManager().GetCurrenMap();
    }

    void Update() {

        if (moveList.Count != 0) {
            Move();
        }

    }

    public void MoveUnit(SquareTile endTile) {

        currentTarget = endTile;
        moveList.Clear();
        FindPath(endTile);

    }

    void Move() {

        //if (bCollidedWithOther) {
        //    MoveUnit(currentTarget);
        //}

        if (moveList.Count != 0) {
            positionToGo = new Vector3(moveList[0].GetWorldPosition().x, transform.position.y, moveList[0].GetWorldPosition().y);
            Movement();
        }
    }

    private void Movement() {
       
        var targetDir = positionToGo - transform.position;
        float rotationStep = rotationSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, rotationStep, 0.0F);
        transform.rotation = Quaternion.LookRotation(newDir);

        float moveStep = moveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, positionToGo, moveStep);

        Vector2 pos = new Vector2(transform.position.x, transform.position.z);

        if ( Vector2.Distance(pos, moveList[0].GetWorldPosition()) <= 0.15f) {
            unitComponent.gridPosition = moveList[0].GetPosition();
            moveList.RemoveAt(0);
        }

    }

    public void SetPath(List<SquareTile> path) {
        moveList = path;
    }

    private void FindPath(SquareTile endTile) {

        SquareTile startTile;
        currentMap.GetMap().TryGetValue(unitComponent.gridPosition, out startTile);
        pathfinder.FindPath(this, startTile, endTile);

    }

    void OnTriggerEnter(Collider other) {

        if (other.gameObject.GetComponent<Unit>()) {
            Debug.Log("collided with - " + other.gameObject.name);
        }
    }

}
