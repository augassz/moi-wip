﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitControler))]
public class Unit : MonoBehaviour {

    public Party unitParty { get; set; }
    private UnitControler controler;

    public Vector2 gridPosition { get; set; }

    bool bSelected;
    GameObject unitSelection;

    public void Init () {

        controler = gameObject.GetComponent<UnitControler>();
    }
	
	void Update () {

    }

    public void SelectUnit() {
        bSelected = true;
        unitSelection = GameManager.instance.GetSelectionLook().ShowSelected(transform.position);
        unitSelection.transform.parent = transform;
    }

    public void DeselectUnit() {
        bSelected = false;
        Destroy(unitSelection);
    }

    public void MoveUnit(SquareTile squareToMoveTo) {

        if(!controler)
            controler = gameObject.GetComponent<UnitControler>();

        controler.MoveUnit(squareToMoveTo);
    }
}
