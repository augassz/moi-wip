﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCreator : ScriptableObject {

    private GameObject unitPrefab;
    private GameObject enemyPrefab;

    MapClass currentMap;


    private void Awake() {
        unitPrefab = (GameObject) Resources.Load("Unit");
        enemyPrefab = (GameObject)Resources.Load("Enemy");
    }

    void Start() {
    }

    public Unit CreateUnit(int xcoord, int ycoord) {

        if(currentMap == null)
            currentMap = GameManager.instance.GetMapManager().GetCurrenMap();

        SquareTile tile = null;
        currentMap.GetMap().TryGetValue(new Vector2(xcoord, ycoord), out tile);
        Vector2 wPos = tile.GetWorldPosition();

        GameObject newUnit = Instantiate(unitPrefab, new Vector3(wPos.x, 0f, wPos.y), Quaternion.identity);
        Unit unit = newUnit.AddComponent<Unit>();
        newUnit.AddComponent<InteractiveObject>();
        unit.gridPosition = new Vector2(xcoord, ycoord);
        return unit;
    }

    public Unit CreateEnemy(int xcoord, int ycoord) {

        if (currentMap == null)
            currentMap = GameManager.instance.GetMapManager().GetCurrenMap();

        SquareTile tile = null;
        currentMap.GetMap().TryGetValue(new Vector2(xcoord, ycoord), out tile);
        Vector2 wPos = tile.GetWorldPosition();

        GameObject newUnit = Instantiate(enemyPrefab, new Vector3(wPos.x, 0f, wPos.y), Quaternion.identity);
        Unit unit = newUnit.AddComponent<Unit>();
        newUnit.AddComponent<InteractiveObject>();
        unit.gridPosition = new Vector2(xcoord, ycoord);
        return unit;
    }

}
