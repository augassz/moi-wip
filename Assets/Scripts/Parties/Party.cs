﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Party {

    public string partyName { get; set; }
    public bool bHumanParty { get; set; }
    public int partyKarma { get; set; }

    private List<Unit> partyUnitsList;

    public Party(bool bHuman, string name, int karma) {
        partyUnitsList = new List<Unit>();
        partyName = name;
        bHumanParty = bHuman;
        partyKarma = karma;
    }

    public List<Unit> GetUnits() {

        return partyUnitsList;
    }

    public void AddUnitToParty(Unit u) {
        partyUnitsList.Add(u);
        u.unitParty = this;
    }

    public void RemoveUnitFromParty(Unit u) {
        partyUnitsList.Remove(u);
    }

    public void RefreshUnitsEnergy() {
        // could be used for hp regen ?
    }


}
